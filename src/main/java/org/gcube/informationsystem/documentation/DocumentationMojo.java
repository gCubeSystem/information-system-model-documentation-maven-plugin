package org.gcube.informationsystem.documentation;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Mojo(name="documentation")
public class DocumentationMojo extends AbstractMojo {
	
	public void execute() throws MojoExecutionException {
		getLog().info( "Hello, world." );
	}

}
